#include "buffer.h"
#include "byte.h"
#include "dns.h"
#include "dd.h"
#include "env.h"
#include "ip4.h"
#include "response.h"
#include "scan.h"
#include "str.h"
#include "strerr.h"
#include "uint32.h"
#include "uint16.h"
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include "sqldns.h"

sql_record sql_records[SQL_RECORD_MAX];

char *fatal = "pgsqldns: fatal: ";
char *warning = "pgsqldns: warning: ";
char *starting = "pgsqldns: starting\n";

struct nameserver
{
  stralloc name;
  char ip[4];
};

static char in_addr_arpa[] = "\7in-addr\4arpa";

static struct timeval now;

static unsigned nameserver_count;
static struct nameserver nameservers[10];

#define DEFAULT_NS_NAME_TTL 259200
#define DEFAULT_NS_IP_TTL 259200
#define DEFAULT_TTL 86400

static uint32 ns_name_ttl;
static uint32 ns_ip_ttl;

#define DEFAULT_SOA_TTL     2560
#define DEFAULT_SOA_REFRESH 4096
#define DEFAULT_SOA_RETRY   256
#define DEFAULT_SOA_EXPIRE  65536
#define DEFAULT_SOA_MINIMUM 2560
#define DEFAULT_SOA_MAILBOX "hostmaster"

#define MAX_DYNAMIC_TTL 3600
#define MIN_DYNAMIC_TTL 2

static uint32 soa_ttl;
static uint32 soa_refresh;
static uint32 soa_retry;
static uint32 soa_expire;
static uint32 soa_minimum;
static stralloc soa_mailbox;

static unsigned long domain_id;
static stralloc domain_name;
static stralloc domain_prefix;

static stralloc scratch;


/*  parse nameserver:ipaddr string */
static int parse_nameserver(unsigned ns, char* env)
{
  unsigned len;
  struct nameserver* nsptr = nameservers+ns;
  
  len = str_chr(env, ':');
  if(!len || len > 255) return 0;
  env[len] = 0;
  if(!name_to_dns(&nsptr->name, env, 0)) return 0;
  env += len+1;
  if(!(len = ip4_scan(env, nsptr->ip))) return 0;
  env += len;
  if(*env) return 0;
  return 1;
}

/* parses nameservers defined in environment */
static void parse_nameservers(void)
{
  unsigned i;
  unsigned j;
  char envname[4] = "NS#";
  for(i = j = 0; i < 10; i++) {
    char* nsline;
    envname[2] = i + '0';
    nsline = env_get(envname);
    if(nsline) {
      if(!parse_nameserver(j, nsline))
	strerr_die3x(111,fatal,"Could not parse nameserver line in ",envname);
      ++j;
    }
  }
  if(j == 0)
    strerr_die2x(111,fatal,"No nameservers were parsed");
  nameserver_count = j;
}

/* Sets ulong variable from environment, using default value if not found */
static void env_get_ulong(char* env, unsigned long* out, unsigned long dflt)
{
  char* tmp = env_get(env);
  if(!tmp) {
       *out = dflt;
       return;
  }
  if(!scan_ulong(tmp, out)) {
    strerr_warn4(warning, "Could not parse $", env, ", ignoring.", 0);
    *out = dflt;
  }
}

/* initialize server on startup */
void initialize(void)
{
  char* env;
  parse_nameservers();
  sql_connect();
  env_get_ulong("NS_IP_TTL",   &ns_ip_ttl,   DEFAULT_NS_IP_TTL);
  env_get_ulong("NS_NAME_TTL", &ns_name_ttl, DEFAULT_NS_NAME_TTL);
  env_get_ulong("SOA_TTL",     &soa_ttl,     DEFAULT_SOA_TTL);
  env_get_ulong("SOA_REFRESH", &soa_refresh, DEFAULT_SOA_REFRESH);
  env_get_ulong("SOA_RETRY",   &soa_retry,   DEFAULT_SOA_RETRY);
  env_get_ulong("SOA_EXPIRE",  &soa_expire,  DEFAULT_SOA_EXPIRE);
  env_get_ulong("SOA_MINIMUM", &soa_minimum, DEFAULT_SOA_MINIMUM);
  env = env_get("SOA_MAILBOX");
  if(!env)
    env = DEFAULT_SOA_MAILBOX;
  if(!name_to_dns(&soa_mailbox, env, 0))
    strerr_die2x(111,fatal,"Could not create initial SOA mailbox string");
}

/* returns true if name is qualified. Name is in dns format */
static int qualified(unsigned char* s)
{
  return s[0] && s[s[0]+1];
}

static int stralloc_cat_domain(stralloc* s, stralloc* domain)
{
  if(!s->len) return 0;
  s->s[s->len-1] = domain->s[0];
  return stralloc_catb(s, domain->s+1, domain->len-1);
}

static int response_addulong(uint32 num)
{
  char bytes[4];
  uint32_pack_big(bytes,num);
  return response_addbytes(bytes, 4);
}

static int response_addushort(uint16 num)
{
  char bytes[2];
  uint16_pack_big(bytes,num);
  return response_addbytes(bytes, 2);
}

static unsigned records;
static stralloc additional;

/* Build a complete A response */
static int response_A(char* q, unsigned long ttl, char ip[4], int additional)
{
  if(!response_rstart(q,DNS_T_A,ttl)) return 0;
  if(!response_addbytes(ip, 4)) return 0;
  response_rfinish(additional ? RESPONSE_ADDITIONAL : RESPONSE_ANSWER);
  ++records;
  return 1;
}

/* Build a complete MX response */
static int response_MX(char* q, unsigned long ttl, unsigned dist, char* name)
{
  if(!response_rstart(q,DNS_T_MX,ttl)) return 0;
  if(!response_addushort(dist)) return 0;
  if(!response_addname(name)) return 0;
  response_rfinish(RESPONSE_ANSWER);
  ++records;
  return 1;
}

static int response_NS(char* q, unsigned long ttl, char* ns, int authority)
{
  if(!response_rstart(q,DNS_T_NS,ttl)) return 0;
  if(!response_addname(ns)) return 0;
  response_rfinish(authority ? RESPONSE_AUTHORITY : RESPONSE_ANSWER);
  ++records;
  return 1;
}

static int response_PTR(char* q, unsigned long ttl, char* name)
{
  if(!response_rstart(q,DNS_T_PTR,ttl)) return 0;
  if(!response_addname(name)) return 0;
  response_rfinish(RESPONSE_ANSWER);
  ++records;
  return 1;
}

static int response_SOA(int authority)
{
  if(!stralloc_copy(&scratch, &soa_mailbox)) return 0;
  if(!qualified(scratch.s))
    if(!stralloc_cat_domain(&scratch, &domain_name)) return 0;

  if(!response_rstart(domain_name.s,DNS_T_SOA,soa_ttl)) return 0;
  if(!response_addname(nameservers[0].name.s)) return 0;
  if(!response_addname(scratch.s)) return 0;
  if(!response_addulong(now.tv_sec)) return 0;
  if(!response_addulong(soa_refresh)) return 0;
  if(!response_addulong(soa_retry)) return 0;
  if(!response_addulong(soa_expire)) return 0;
  if(!response_addulong(soa_minimum)) return 0;
  response_rfinish(authority ? RESPONSE_AUTHORITY : RESPONSE_ANSWER);
  ++records;
  return 1;
}

static int response_TXT(char* q, unsigned long ttl, char* text)
{
  if(!response_rstart(q,DNS_T_TXT,ttl)) return 0;
  if(!response_addname(text)) return 0;
  response_rfinish(RESPONSE_ANSWER);
  ++records;
  return 1;
}

#define LOG(MSG) buffer_putsflush(buffer_1, MSG "\n")

static int dns_domain_join(stralloc* prefix, stralloc* domain)
{
  prefix->s[prefix->len-1] = domain->s[0];
  return stralloc_catb(prefix, domain->s+1, domain->len-1);
}

static int query_domain(char* q)
{
  unsigned int suffix;
  if(!sql_select_domain(q, &domain_id, &scratch)) return 0;
  if(!name_to_dns(&domain_name, scratch.s, 0)) return 0;
  
  suffix = dns_domain_suffixpos(q, domain_name.s);
  
  /* domain now points to the suffix part of the input query */
  /* Copy the first part of the query into domain_prefix */
  if(!stralloc_copyb(&domain_prefix, q, suffix)) return 0;
  if(!stralloc_0(&domain_prefix)) return 0;
  
  return 1;
}

static int sent_NS;

/* The call to dns_random will cause the sorted data set to have like items
 * put into a randomized order */
static int cmp_records(const sql_record* a, const sql_record* b)
{
  int c = a->type - b->type;
  if(!c)
    c = dns_random(3) - 1;
  return c;
}

/* I used a simple insertion sort here since:
 * 1. the number of records is small
 * 2. qsort has a high overhead time for small counts
 * 3. comparisons are fast
 * 4. swapping elements (due to the record size) is slow */
static void sort_sql_records(unsigned count)
{
  unsigned i;
  for(i = 0; i < count-1; i++) {
    sql_record* jmax = sql_records + i;
    unsigned j;
    for(j = i+1; j < count; j++)
      if(cmp_records(sql_records+j, jmax) < 0)
	jmax = sql_records+j;
    if(jmax != sql_records+i) {
      sql_record tmp = sql_records[i];
      sql_records[i] = *jmax;
      *jmax = tmp;
    }
  }
}

static int respond_nameservers(int authority)
{
  unsigned i;
  if(!sent_NS) {
    for(i = 0; i < nameserver_count; i++)
      if(!response_NS(domain_name.s, ns_name_ttl,
		      nameservers[i].name.s, authority))
	return 0;
  }
  sent_NS = 1;
  return 1;
}

static int lookup_A;
static int lookup_MX;
static int lookup_NS;
static int lookup_PTR;
static int lookup_SOA;
static int lookup_TXT;

static unsigned tuples;

static int query_forward(char* q)
{
  unsigned row;
  unsigned int suffix;
  sql_record* rec;
  
  tuples = sql_select_entries(domain_id, &domain_prefix);
  if(!tuples) {
    response_nxdomain();
    return 1;
  }

  sort_sql_records(tuples);
  if(domain_prefix.len == 1) {
    if(lookup_SOA)
      if(!response_SOA(0)) return 0;
    if(lookup_NS)
      if(!respond_nameservers(0)) return 0;
  }

  /* Handle timestamps:
   * if TTL=0, timestamp indicates when the record expires
   * otherwise, timestamp indicates when the record should appear */
  for(row = 0; row < tuples; row++) {
    rec = sql_records + row;
    if(rec->timestamp) {
      if(rec->ttl) {
	if(rec->timestamp > now.tv_sec)
	  rec->type = 0;
      }
      else {
	if(rec->timestamp <= now.tv_sec)
	  rec->type = 0;
	else {
	  rec->ttl = rec->timestamp - now.tv_sec;
	  if(rec->ttl > MAX_DYNAMIC_TTL) rec->ttl = MAX_DYNAMIC_TTL;
	  if(rec->ttl < MIN_DYNAMIC_TTL) rec->ttl = MIN_DYNAMIC_TTL;
	}
      }
    }
    else if(!rec->ttl)
      rec->ttl = DEFAULT_TTL;
  }
  
  for(row = 0, rec = sql_records; row < tuples; row++, rec++) {
    if(lookup_A && rec->type == DNS_NUM_A)
      if(!response_A(q, rec->ttl, rec->ip, 0)) return 0;
    if(lookup_MX && rec->type == DNS_NUM_MX) {
      if(!name_to_dns(&scratch, rec->name.s, 0)) return 0;
      if(!qualified(scratch.s))
	if(!dns_domain_join(&scratch, &domain_name)) return 0;
      if(!response_MX(q, rec->ttl, rec->distance, scratch.s)) return 0;
      /* If the domain of the added name matches the current domain,
       * add an additional A record */
      suffix = dns_domain_suffixpos(scratch.s, domain_name.s);
      if(suffix) {
	if(!stralloc_catb(&additional, scratch.s, suffix))
	  return 0;
	if(!stralloc_0(&additional)) return 0;
      }
    }
    if(lookup_TXT && rec->type == DNS_NUM_TXT) {
      if(!name_to_dns(&scratch, rec->name.s, 1)) return 0;
      if(!response_TXT(q, rec->ttl, scratch.s)) return 0;
    }
  }
  return 1;
}

static int query_reverse(unsigned char* q)
{
  /* Convert the numerical parts of q to an ip array
   * If one of the 4 parts is bad, give a NXDOMAIN response */
  char ip[4];
  unsigned parts;
  char* ptr;
  sql_record* rec;
  
  if(!lookup_PTR)
    return response_SOA(1);
  
  ptr = q;
  for(parts = 0; parts < 4; parts++) {
    unsigned len = *ptr;
    unsigned long num;
    if(!byte_diff(ptr, sizeof in_addr_arpa, in_addr_arpa)) break;
    if(scan_ulong(ptr+1, &num) != len || num > 255) {
      response_nxdomain();
      return 1;
    }
    ip[3-parts] = num;
    ptr += len + 1;
  }
  /* If less than 4 parts were converted:
   *   if domain_prefix is empty:
   *     produce an SOA response
   *   else:
   *     produce a NXDOMAIN response */
  if(parts < 4 || byte_diff(ptr, sizeof in_addr_arpa, in_addr_arpa)) {
    if(domain_prefix.len == 1) {
      if(!response_SOA(0)) return 0;
      if(!respond_nameservers(0)) return 0;
    }
    else
      response_nxdomain();
    return 1;
  }

  tuples = sql_select_ip4(ip);
  if(!tuples) {
                 response_nxdomain();
		 return 1;
  }		 

  rec = &sql_records[0];
  if(!name_to_dns(&scratch, rec->name.s, 0)) return 0;
  if(!qualified(scratch.s))
  {
       printf("Warning, unqualificated reply from PTR!\n");
       if(!dns_domain_join(&scratch, &domain_name)) return 0;
  }
  return response_PTR(q, rec->ttl, scratch.s);
}

static int respond_additional(void)
{
  unsigned i;
  if(additional.len) {
    /* Optimization: Don't do an additional SQL query if
     * the additional record is exactly the same as the original query */
    if(additional.len != domain_prefix.len ||
       byte_diff(additional.s, domain_prefix.len, domain_prefix))
       tuples = sql_select_entries(domain_id, &additional);
    for(i = 0; i < tuples; i++) {
      sql_record* rec = &sql_records[i];
      if(rec->type == DNS_NUM_A) {
	if(!dns_domain_join(&rec->prefix, &domain_name)) return 0;
	if(!response_A(rec->prefix.s,rec->ttl,rec->ip,1)) return 0;
      }
    }
  }
  for(i = 0; i < nameserver_count; i++) {
    if(!response_A(nameservers[i].name.s, ns_ip_ttl,
		   nameservers[i].ip, 1))
      return 0;
  }
  return 1;
}

int respond(char *q, unsigned char qtype[2] /*, char srcip[4] */)
{
  char seed[128];

  gettimeofday(&now, 0);
  dns_random_init(seed);
  
  lookup_A=lookup_MX=lookup_NS=lookup_PTR=lookup_SOA=lookup_TXT=sent_NS=0;
  
  switch((qtype[0] << 8) | qtype[1]) {
  case DNS_NUM_ANY:
    lookup_A=lookup_MX=lookup_NS=lookup_SOA=lookup_PTR=lookup_TXT=1; break;
  case DNS_NUM_A:   lookup_A = 1; break;
  case DNS_NUM_MX:  lookup_MX = 1; break;
  case DNS_NUM_NS:  lookup_NS = 1; break;
  case DNS_NUM_PTR: lookup_PTR = 1; break;
  case DNS_NUM_SOA: lookup_SOA = 1; break;
  case DNS_NUM_TXT: lookup_TXT = 1; break;
  default:
    response[2] &= ~4;
    response[3] &= ~15;
    response[3] |= 5;
    return 1;
  }

  if(!query_domain(q)) return 0;
  
  records = 0;
  if(!stralloc_copys(&additional, "")) return 0;

  if(dns_domain_suffix(q, in_addr_arpa)) {
    if(!query_reverse(q)) return 0;
  }
  else {
    if(!query_forward(q)) return 0;
  }
  
  if(!records) {
    if(!response_SOA(1)) return 0;
  }
  else {
    if(!respond_nameservers(1)) return 0;
    if(!respond_additional()) return 0;
  }
  return 1;
}
