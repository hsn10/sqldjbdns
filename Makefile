PACKAGE = sqldjbdns
VERSION = 0.74

djbdns = djbdns-1.05

TARFILE = ${PACKAGE}-${VERSION}.tar
DOC = ANNOUNCEMENT NEWS README STATUS TODO

CC?= cc
CFLAGS?= -O2 -W -Wall -ggdb
CPPFLAGS+=-I$(djbdns) -USQL_LOG -I`pg_config --includedir` -DSQLCACHE -UQUERY_LOG -UQUERY_DEBUG -D_REENTRANT
CFLAGS+=  ${CPPFLAGS}
install = ${INSTALL}

install_prefix?=
prefix = ${PREFIX}
bindir = $(install_prefix)$(prefix)/bin

PROGS = pgsqldns pgsqldns-conf dnsbench pgsqldnscache

default: $(PROGS)

patch:
	-cd $(djbdns) && git diff  1.05 > ../djbdns-1.05.patch
tar:	patch
	mkdir ${PACKAGE}-${VERSION}
	cp -p Makefile *.h *.c *.patch *.html *.sql *.spec ${DOC} ${PACKAGE}-${VERSION}
	tar cvf ${TARFILE} ${PACKAGE}-${VERSION}
	rm -r ${PACKAGE}-${VERSION} 
	gzip -9f ${TARFILE}
	-advdef -z4 ${TARFILE}.gz

pgsqldns: pgsqldns.o sqldns.o sqlschema.o sqlutils.o $(djbdns)/dns.a
	cd $(djbdns) && ./load ../pgsqldns ../sqldns.o ../sqlschema.o \
	        ../sqlutils.o \
		server.o iopause.o response.o droproot.o qlog.o prot.o dd.o \
		dns.a env.a cdb.a alloc.a buffer.a unix.a byte.a libtai.a \
		`cat socket.lib` -L`pg_config --libdir` -lpq

pgsqldns-conf: pgsqldns-conf.o $(djbdns)/dns.a
	cd $(djbdns) && ./load ../pgsqldns-conf generic-conf.o \
		auto_home.o buffer.a byte.a alloc.a unix.a

dnsbench: dnsbench.o $(djbdns)/dns.a
	cd $(djbdns) && ./load ../dnsbench dns.a env.a \
		alloc.a buffer.a unix.a byte.a iopause.o \
		libtai.a `cat socket.lib`

pgsqldnscache: sqlcacheschema.o sqlutils.o pgsqldns.o query-sql.o pgsqldnscache.o \
	log-sql.o $(djbdns)/dns.a $(djbdns)/droproot.o
	cd $(djbdns) && ./load ../pgsqldnscache droproot.o okclient.o \
	../log-sql.o \
	cache.o ../query-sql.o response.o dd.o roots.o iopause.o prot.o dns.a \
	../sqlcacheschema.o ../sqlutils.o \
	env.a alloc.a libtai.a  byte.a sig.a  `cat socket.lib` \
	../pgsqldns.o buffer.a unix.a -L`pg_config --libdir` -lpq

install: $(PROGS)
	$(install) -d $(bindir)
	$(install) $(PROGS) $(bindir)

dnsbench.o: dnsbench.c Makefile
pgsqldns.o: pgsqldns.c sqldns.h $(djbdns)/uint64.h Makefile
pgsqldns-conf.o: pgsqldns-conf.c $(djbdns)/uint64.h Makefile
sqldns.o: sqldns.c sqldns.h $(djbdns)/uint64.h Makefile
sqlschema.o: sqlschema.c sqldns.h $(djbdns)/uint64.h Makefile
sqlcacheschema.o: sqlcacheschema.c sqldns.h $(djbdns)/uint64.h Makefile
sqlutils.o: sqlutils.c sqldns.h $(djbdns)/uint64.h Makefile
query-sql.o: $(djbdns)/query.c $(djbdns)/uint64.h  Makefile
	$(CC) $(CFLAGS) -c $(djbdns)/query.c -o $@
pgsqldnscache.o: $(djbdns)/dnscache.c $(djbdns)/uint64.h Makefile
	$(CC) $(CFLAGS) -c $(djbdns)/dnscache.c -o $@
log-sql.o: $(djbdns)/log.c $(djbdns)/uint64.h Makefile
	$(CC) $(CFLAGS) -c $(djbdns)/log.c -o $@

$(djbdns)/dns.a: $(djbdns)/dns_domain.c
	$(MAKE) -C $(djbdns)

$(djbdns)/uint64.h: $(djbdns)/tryulong64.c
	$(MAKE) -C $(djbdns)

clean:
	rm -f *.o $(PROGS) ${PACKAGE}-*.tar.gz tags
