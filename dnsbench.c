#include "dns.h"
#include "scan.h"
#include "strerr.h"

int main(int argc, char* argv[])
{
  unsigned long i;
  int j;
  stralloc out = {0,0,0};
  stralloc fqdn = {0,0,0};
  unsigned long loops;
  
  if(argc < 3) 
      strerr_die2x(1,argv[0]," <loops> <dns name> ...");

  if(argv[1][scan_ulong(argv[1], &loops)])
      strerr_die4x(1,argv[0]," Can not parse ",argv[1]," as number.");

  for(i = 0; i < loops; i++) {
    for(j = 2; j < argc; j++) {
      stralloc_copys(&fqdn, argv[j]);
      stralloc_copys(&out, "");
      dns_ip4(&out, &fqdn);
    }
  }
  return 0;
}
