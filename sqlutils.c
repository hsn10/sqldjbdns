#include "stralloc.h"
#include "ip4.h"
#include "scan.h"
#include "uint32.h"
#include "sqldns.h"

int sql_fetch_ip4(unsigned row, unsigned col, char ip[4])
{
  char* data;
  unsigned length;

  if((length = sql_fetch(row, col, &data)) == SQLNULL || !length) return 0;
  return ip4_scan(data, ip) == length;
}

int sql_fetch_ulong(unsigned row, unsigned col, unsigned long* result)
{
  char* data;
  unsigned length;

  if((length = sql_fetch(row, col, &data)) == SQLNULL || !length) return 0;
  return scan_ulong(data, result) == length;
}

int sql_fetch_stralloc(unsigned row, unsigned col, stralloc* result)
{
  char* data;
  unsigned length;

  if((length = sql_fetch(row, col, &data)) == SQLNULL) return 0;
  if(!stralloc_copyb(result, data, length)) return 0;
  return stralloc_0(result);
}

int stralloc_appendchar(stralloc* s, char b)
{
  char tmp[1];
  tmp[0] = b;
  return stralloc_append(s, tmp);
}

int stralloc_catoctal(stralloc* s, unsigned char c)
{
  char tmp[5];
  tmp[0] = '\\';
  tmp[1] = '0' + ((c >> 6) & 7);
  tmp[2] = '0' + ((c >> 3) & 7);
  tmp[3] = '0' + (c & 7);
  tmp[4] = 0;
  return stralloc_cats(s, tmp);
}

/* Append the binary DNS name as text to the stralloc */
int stralloc_cat_dns_to_sql(stralloc* s, const char* name)
{
  int dot = 0;
  if(!stralloc_append(s, "'")) return 0;
  while(*name) {
    unsigned len;
    if(dot)
      if(!stralloc_append(s, ".")) return 0;
    for(len = *name++; len; --len, ++name) {
      char ch = *name;
      if(ch >= 'A' && ch <= 'Z') ch += 32;
      if((ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9') ||
	 ch == '-' || ch == '_') {
	if(!stralloc_appendchar(s, ch)) return 0;
      }
      else
	if(!stralloc_catoctal(s, ch)) return 0;
    }
    dot = 1;
  }
  return stralloc_append(s, "'");
}

/* Append the binary DNS name as text to the stralloc */
int stralloc_cat_string_to_sql(stralloc* s, const char* name)
{
  unsigned len;
  unsigned i;
  
  if(!stralloc_append(s, "'")) return 0;
  len = *(unsigned char *)name;
  for(i=0;i<len;i++) {
      char ch;
      ch = *(name+i+1);
      if(ch >= 32  && ch < 127 && ch !='\'' && ch !='\\') {
	if(!stralloc_appendchar(s, ch)) return 0;
      }
      else
	if(!stralloc_catoctal(s, ch)) return 0;
  }
  return stralloc_append(s, "'");
}

/* Convert the given text domain name into DNS binary format. */
int name_to_dns(stralloc* dns, char* name, int split)
{
  if(!stralloc_copys(dns, "")) return 0;
  while(*name == '.')
    ++name;
  while(*name) {
    char* start = name;
    unsigned char tmp[1];
    unsigned length;

    while(*name && *name != '.')
      ++name;
    length = name - start;
    if(length >= 127 && !split) return 0;
    while(length) {
      tmp[0] = length > 127 ? 127 : length;
      if(!stralloc_catb(dns, tmp, 1)) return 0;
      if(!stralloc_catb(dns, start, tmp[0])) return 0;
      length -= tmp[0];
      start += tmp[0];
    }
    while(*name == '.')
      ++name;
  }
  return stralloc_0(dns);
}
