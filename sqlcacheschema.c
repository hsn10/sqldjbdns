#include "buffer.h"
#include "dns.h"
#include "env.h"
#include "ip4.h"
#include "scan.h"
#include "stralloc.h"
#include "strerr.h"
#include "uint16.h"
#include "uint32.h"
#include "sqldns.h"
#include "byte.h"
#include "cache.h"
#include "dns.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>


static stralloc sql_query = {0,0,0};

/* inserts an entry into database */
/*
Table "dnscache"
  Column  |            Type             | Modifiers
----------+-----------------------------+-----------
 domain   | text                        | not null
 type     | integer                     | not null
 expire   | timestamp without time zone | not null
 ip       | inet                        |
 distance | integer                     |
 name     | text                        |
*/

static stralloc scratch =  { 0,0,0};
static stralloc scratch2 = { 0,0,0};

unsigned int commit_group = 100;
unsigned long db_motion = 0;
static unsigned int current_group = 0;
static volatile int in_transaction = 0;

#define IP6_FMT    sizeof "ffff:ffff:ffff:ffff:ffff:ffff:255.255.255.255"+1

extern void log_clean(unsigned int entries);

unsigned int sql_cleandb(int keep_expired)
{
    unsigned long cleaned;

    if(in_transaction) return 0;
    in_transaction = 1;
    if(keep_expired)
    { 
        sql_exec("SELECT cleandnscache(false)");
    }
    else
    {
        sql_exec("SELECT cleandnscache(true)");
    }
    if(!sql_fetch_ulong(0, 0, &cleaned)) cleaned=0;
    sql_exec("COMMIT;BEGIN");
    in_transaction = 0;
    log_clean(cleaned);
    return cleaned;
}

int sql_flushdb()
{
    if(in_transaction) return 0;
    if(current_group > 0 )
    {
	    sql_exec("COMMIT;BEGIN");
	    current_group=0;
    }
    return 1;
}

void sql_prepare()
{
  sql_exec("PREPARE i1 (text,int2,timestamp without time zone,inet,int2,text)"
           " AS INSERT INTO dnscache (domain,type,expire,ip,distance,name)"
	   " VALUES ($1,$2,$3,$4,$5,$6)");
  sql_exec("BEGIN");
}

int insert_entry(const char t[2],const char *d,const char *buf,unsigned int len, uint32 ttl)
{
    uint16 type;
    uint16 distance;
    unsigned int i=0,iplen;
    char ip[IP6_FMT];

    uint16_unpack_big(t,&type);
    if(type>32767) return 0;
    if(!stralloc_copys(&scratch,
		     "EXECUTE i1("
		     ))
	return 0;
    if(!stralloc_cat_dns_to_sql(&scratch, d)) return 0;
    if(!stralloc_cats(&scratch, ",")) return 0;
    if(!stralloc_catint(&scratch,type)) return 0;
    if(!stralloc_cats(&scratch, ",")) return 0;
    if(!stralloc_cats(&scratch, "now()+'")) return 0;
    if(!stralloc_catulong0(&scratch,ttl,0)) return 0;
    if(!stralloc_cats(&scratch, "'::interval,")) return 0;
#ifdef QUERY_LOG    
    printf("Inserting record: %s, type: %u, len: %u, ttl: %u\n",d,type,len,(unsigned int)ttl);
    fflush(stdout);
#endif    
    if(len==0)
    {
	/* we can handle zero sized record of any type */
	if(!stralloc_cats(&scratch, "NULL,NULL,NULL)")) return 0;
        if(!stralloc_0(&scratch)) return 0;
	in_transaction=1;
	sql_exec(scratch.s);
	in_transaction=0;
	current_group++;
	db_motion++;
	return 1;
    }
    in_transaction=1;
    while(i<len)
    {
	if(!stralloc_copyb(&sql_query,scratch.s,scratch.len))
	    return 0;
	switch(type)
	{
	    case DNS_NUM_A:
	           iplen=ip4_fmt(ip,buf+i);
                   if(!stralloc_appendchar(&sql_query,'\'')) return 0;
	           if(!stralloc_catb(&sql_query,ip,iplen)) return 0;
                   if(!stralloc_cats(&sql_query,"\',NULL,NULL")) return 0;
		   i+=4;
		   break;
	    case DNS_NUM_AAAA:
	           i+=2;
	           if(inet_ntop(AF_INET6,buf+i,ip,sizeof ip)==NULL)
		   {
		       i+=16;
		       perror("ntop");
		       continue;
		   }
                   if(!stralloc_appendchar(&sql_query,'\'')) return 0;
	           if(!stralloc_cats(&sql_query,ip)) return 0;
                   if(!stralloc_cats(&sql_query,"\',NULL,NULL")) return 0;
		   i+=16;
		   break;
	    case DNS_NUM_NS:
	    case DNS_NUM_CNAME:
	    case DNS_NUM_PTR:
                   if(!stralloc_cats(&sql_query,"NULL,NULL,")) return 0;
	           if(!stralloc_cat_dns_to_sql(&sql_query, buf+i)) return 0;
		   i+=dns_domain_length(buf+i);
		   break;
            case DNS_NUM_TXT:		   
	           i+=2;
		   if(!stralloc_cats(&sql_query,"NULL,NULL,")) return 0;
		   if(!stralloc_cat_string_to_sql(&sql_query, buf+i)) return 0;
		   i+=*(unsigned char *)(buf+i);
		   i++;
		   break;
	    case DNS_NUM_MX:
                   if(!stralloc_cats(&sql_query,"NULL,")) return 0;
                   uint16_unpack_big(buf+i,&distance);
		   if(distance>32767) distance=32767;
		   i+=2;
                   if(!stralloc_catulong0(&sql_query,distance,0)) return 0;
                   if(!stralloc_cats(&sql_query,",")) return 0;
	           if(!stralloc_cat_dns_to_sql(&sql_query, buf+i)) return 0;
		   i+=dns_domain_length(buf+i);
		   break;
	    default:
	           printf("Don't know how to handle record of this type\n");
		   in_transaction = 0;
		   return 0;
	}
	if(!stralloc_catb(&sql_query, ")",2)) return 0;
	sql_exec(sql_query.s);
	current_group++;
	db_motion++;
    }
    if(current_group>=commit_group)
    {
        sql_exec("COMMIT;BEGIN");
        current_group=0;
    }
    in_transaction = 0;
    return 1;
}

extern char save_buf[8192];
extern unsigned int save_len;

unsigned int load_cache(int load_expired)
{
    unsigned int records;
    unsigned int i,tuples;

    unsigned long type,distance;
    unsigned long ttl;
    char ip[16];
    unsigned int keylen;
    char key[257];
    
    // POZOR! pred pouzitim je nutne se zbavit duplicit
    if(!stralloc_copys(&sql_query,"SELECT domain,type,")) return 0;
    if(!load_expired)
    {
         if(!stralloc_cats(&sql_query,
		     "date_part('epoch',(expire-now())::interval(0)),"
           )) return 0;
    }
    else
    {
         if(!stralloc_cats(&sql_query,"86400,"))
              return 0;
    }
    if(!stralloc_cats(&sql_query, "ip,distance,name FROM dnscache "))
       return 0;
    if(!load_expired) 
         if(!stralloc_cats(&sql_query,
		     "WHERE expire>now() "))
	     return 0;
    if(!stralloc_cats(&sql_query,"ORDER BY type,domain"))
	return 0;
    if(!stralloc_0(&sql_query)) return 0;
    sql_exec(sql_query.s);
    keylen = 0;
    save_len = 0;
    tuples = sql_ntuples();
    if(!tuples) return 0;
    if(!stralloc_copys(&scratch,"")) return 0;

    for(i = records = 0; i < tuples ; i++) {
	if(!sql_fetch_stralloc(i, 0, &sql_query)) continue;
        if(!name_to_dns(&scratch,sql_query.s,0)) continue;
	/* zjistit zda je nutne udelat flush zaznamu */
        if (!byte_equal(key+2,scratch.len,scratch.s)) {
	    if(keylen>0)
	    {
		/* ulozit zaznam */
                cache_set(key,keylen + 2,save_buf,save_len,ttl);
		records++;
	    }
	    // printf("new entry %s\n",sql_query.s);
	    /* vygenerovat novy klic */
	    if(!sql_fetch_ulong(i, 1, &type)) continue;
            uint16_pack_big(key,type);
	    keylen=scratch.len;
	    /*
	    if(keylen!=dns_domain_length(scratch.s))
		printf("size is different!\n");
	    */
            byte_copy(key+2,scratch.len,scratch.s);
	    save_len=0;
	}
	if(!sql_fetch_ulong(i, 2, &ttl)) continue;
	/* zjistit zda mame NULL zaznam nebo ne */
	/* ip==null and name==null */
	if(!sql_fetch_stralloc(i,5, &sql_query)  &&
	   !sql_fetch_stralloc(i,3,&scratch2)
	  )
	 {
	     /* create zero-length record */
	     save_len=0;
	     continue;
	 }
	switch(type)
	{
	    case DNS_NUM_NS:
	    case DNS_NUM_CNAME:
	    case DNS_NUM_PTR:
                 if(!name_to_dns(&scratch2,sql_query.s,0)) continue;
                 byte_copy(save_buf+save_len,scratch2.len,scratch2.s);
		 save_len+=scratch2.len;
		 continue;
	    case DNS_NUM_A:
                 if(!ip4_scan(scratch2.s, ip))
		     continue;
                 byte_copy(save_buf+save_len,4,ip);
		 save_len+=4;
		 continue;
            case DNS_NUM_AAAA:
                 uint16_pack_big(save_buf+save_len,16);
		 save_len+=2;
	         if(inet_pton(AF_INET6,scratch2.s,save_buf+save_len)<=0)
		 {
		       perror("pton");
		       save_len-=2;
		       continue;
		 }
		 save_len+=16;
	         break;
            case DNS_NUM_TXT:
                 uint16_pack_big(save_buf+save_len,sql_query.len);
		 save_len+=2;
		 save_buf[save_len]=sql_query.len-1; /* ignore trailing \0 */
		 save_len++;
                 byte_copy(save_buf+save_len,sql_query.len-1,sql_query.s);
                 save_len+=sql_query.len-1;
	         break;
            case DNS_NUM_MX:
	         if(!sql_fetch_ulong(i, 4, &distance)) continue;
                 uint16_pack_big(save_buf+save_len,distance);
		 save_len+=2;
                 if(!name_to_dns(&scratch2,sql_query.s,0)) continue;
                 byte_copy(save_buf+save_len,scratch2.len,scratch2.s);
		 save_len+=scratch2.len;
		 continue;
	    default:
	         printf("don't know how to load record type %ld\n",type);
		 keylen=0;
	}
    }
    if(keylen>0)
    {
        /* ulozit posledni zaznam */
	cache_set(key,keylen + 2,save_buf,save_len,ttl);
	records++;
    }
    /* clean result set from memory */
    sql_exec("SELECT distance FROM dnscache WHERE domain IS NULL");
    /* remove possible stale locks */
    sql_exec("COMMIT;BEGIN");
    return records;
}
