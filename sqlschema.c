#include "buffer.h"
#include "dns.h"
#include "env.h"
#include "ip4.h"
#include "scan.h"
#include "stralloc.h"
#include "strerr.h"
#include "uint32.h"
#include "sqldns.h"
#include <stdio.h>

static stralloc sql_query = {0,0,0};

int sql_select_domain(char* domain, unsigned long* id, stralloc* name)
{
  unsigned i;
  
  if(!stralloc_copys(&sql_query,
		     "SELECT id,name "
		     "FROM domain "
		     "WHERE name="))
    return 0;
  for(i = 0; *domain; ++i, domain += *domain+1) {
    if(i)
      if(!stralloc_cats(&sql_query, " OR name=")) return 0;
    if(!stralloc_cat_dns_to_sql(&sql_query, domain)) return 0;
  }
  if(!stralloc_cats(&sql_query,
		    " ORDER BY length(name) DESC LIMIT 1"))
    return 0;
  if(!stralloc_0(&sql_query)) return 0;
  sql_exec(sql_query.s);
  if(sql_ntuples() != 1) return 0;
  if(!sql_fetch_ulong(0, 0, id)) return 0;
  if(!sql_fetch_stralloc(0, 1, name)) return 0;
  return (name->len > 1);
}

static int stralloc_cat_prefixes(stralloc* q, stralloc* prefixes)
{
  char* ptr;
  unsigned len;
  int first;
  unsigned left;
  
  ptr = prefixes->s;
  left = prefixes->len;
  first = 1;

  while(left) {
    if(!first && !stralloc_cats(q, " OR ")) return 0;
    if(!stralloc_cats(q, "prefix=")) return 0;
    if(!stralloc_cat_dns_to_sql(q, ptr)) return 0;
    len = dns_domain_length(ptr);
    ptr += len;
    left -= len;
    first = 0;
  }
  return 1;
}

unsigned sql_select_entries(unsigned long domain, stralloc* prefixes)
{
  unsigned tuples;
  unsigned rtuples;
  unsigned i;
  sql_record* rec;
  
  if(!prefixes->len) return 0;

  if(!stralloc_copys(&sql_query,
		     "SELECT prefix,type,ttl,date_part('epoch',timestamp),"
		     "ip,distance,name "
		     "FROM entry "
		     "WHERE domain=")) return 0;
  if(!stralloc_catulong0(&sql_query, domain, 0)) return 0;
  if(!stralloc_cats(&sql_query, " AND (")) return 0;
  if(!stralloc_cat_prefixes(&sql_query, prefixes)) return 0;
  if(!stralloc_catb(&sql_query, ")", 2)) return 0;
  sql_exec(sql_query.s);

  tuples = sql_ntuples();
  if(!tuples) return 0;

  rec = sql_records;
  for(i = rtuples = 0; i < tuples && rtuples < SQL_RECORD_MAX; i++) {
    if(!sql_fetch_stralloc(i, 0, &rec->prefix)) continue;
    if(!sql_fetch_ulong(i, 1, &rec->type)) continue;
    if(!sql_fetch_ulong(i, 2, &rec->ttl)) rec->ttl = 0;
    if(!sql_fetch_ulong(i, 3, &rec->timestamp)) rec->timestamp = 0;
    
    switch(rec->type) {
    case DNS_NUM_A:
      if(!sql_fetch_ip4(i, 4, rec->ip)) continue;
      break;
    case DNS_NUM_MX:
      if(!sql_fetch_ulong(i, 5, &rec->distance)) continue;
      if(!sql_fetch_stralloc(i, 6, &rec->name)) continue;
      break;
    case DNS_NUM_TXT:
      if(!sql_fetch_stralloc(i, 6, &rec->name)) continue;
      break;
    default:
      continue;
    }
    ++rec;
    ++rtuples;
  }
  if(rtuples < SQL_RECORD_MAX)
    rec->type = 0;
  /* Return a single bogus record if no data was produced
   * but the prefix was found */
  if(!rtuples) {
    rec->type = 0;
    ++rtuples;
  }
  return rtuples;
}

/* search for A record with specified IP */
unsigned sql_select_ip4(char ip[4])
{
  sql_record* rec;
  char ipstr[IP4_FMT];
  unsigned iplen,i,tuples;
  unsigned rtuples=0;
  
  iplen=ip4_fmt(ipstr, ip);
  if(!stralloc_copys(&sql_query,
	     "SELECT prefix,domain.name,ttl,date_part('epoch',timestamp) "
	     "FROM domain,entry "
	     "WHERE entry.domain=domain.id "
	     "AND type=1 "
	     "AND ip='")) return 0;
  if(!stralloc_catb(&sql_query, ipstr, iplen)) return 0;
  if(!stralloc_catb(&sql_query, "'",2)) return 0;
  sql_exec(sql_query.s);
  tuples=sql_ntuples();
  if(!tuples) return 0;
  rec = sql_records;
  for(i = rtuples = 0; i < tuples && rtuples < SQL_RECORD_MAX; i++) {
    if(!sql_fetch_ulong(i, 2, &rec->ttl)) rec->ttl = 0;
    if(!sql_fetch_ulong(i, 3, &rec->timestamp)) rec->timestamp = 0;
    rec->type = DNS_NUM_PTR;
    if(!sql_fetch_stralloc(i, 0, &rec->prefix)) continue;
    if(rec->prefix.len > 1) {
       if(!stralloc_copyb(&sql_query, rec->prefix.s, rec->prefix.len-1)) return 0;
       if(!stralloc_appendchar(&sql_query, '.')) return 0;
    }
    else {
      if(!stralloc_copys(&sql_query, "")) return 0;
    }
    if(!sql_fetch_stralloc(i, 1, &rec->prefix)) continue;
    if(!stralloc_copyb(&rec->name,sql_query.s,sql_query.len)) return 0;
    if(!stralloc_cats(&rec->name,rec->prefix.s)) return 0;
    if(!stralloc_0(&rec->name)) return 0;
    ++rec;
    ++rtuples;
  }
  
  return rtuples;
}
