#ifndef SQLDNS_H
#define SQLDNS_H

#define DNS_NUM_A 1
#define DNS_NUM_NS 2
#define DNS_NUM_CNAME 5
#define DNS_NUM_SOA 6
#define DNS_NUM_PTR 12
#define DNS_NUM_HINFO 13
#define DNS_NUM_MX 15
#define DNS_NUM_TXT 16
#define DNS_NUM_AAAA 28
#define DNS_NUM_ANY 255

struct sql_record 
{
  stralloc prefix;
  unsigned long type;
  unsigned long ttl;
  char ip[4];
  unsigned long distance;
  stralloc name;
  unsigned long timestamp; /* no 64bit support in scan_yet */
};
typedef struct sql_record sql_record;

#define SQL_RECORD_MAX 256
extern sql_record sql_records[SQL_RECORD_MAX];

#define SQLNULL ((unsigned)-1)

/* Defined by the low-level SQL module */
void sql_connect(void);
void sql_disconnect(void);
void sql_exec(const char* q);
unsigned sql_fetch(unsigned row, unsigned col, char** result);
unsigned sql_ntuples(void);

/* Defined by the SQL schema module */
int sql_select_domain(char* domain, unsigned long* id, stralloc* name);
unsigned sql_select_entries(unsigned long domain, stralloc* prefixes);
unsigned sql_select_ip4(char ip[4]);

/* Defined by SQL utils module */
int sql_fetch_ip4(unsigned row, unsigned col, char ip[4]);
int sql_fetch_ulong(unsigned row, unsigned col, unsigned long* result);
int sql_fetch_stralloc(unsigned row, unsigned col, stralloc* result);
int stralloc_appendchar(stralloc* s, char b);
int stralloc_catoctal(stralloc* s, unsigned char c);
int stralloc_cat_dns_to_sql(stralloc* s, const char* name);
int name_to_dns(stralloc* dns, char* name, int split);
int stralloc_cat_string_to_sql(stralloc* s, const char* name);

/* sql cache schema */
int insert_entry(const char t[2],const char *d,const char *buf,unsigned int len, uint32 ttl);
int sql_flushdb(void);
unsigned int sql_cleandb(int keep_expired);
unsigned int load_cache(int load_expired);
void sql_prepare(void);
extern unsigned int commit_group;
extern unsigned long db_motion;
#endif
