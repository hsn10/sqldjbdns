#include <pwd.h>
#include "strerr.h"
#include "exit.h"
#include "auto_home.h"
#include "stralloc.h"
#include "generic-conf.h"

#define FATAL "pgsqldns-conf: fatal: "

void usage(void)
{
  strerr_die1x(100,"pgsqldns-conf: usage: pgsqldns-conf acct logacct /pgsqldns myip ns1:ip ...");
}

void nomem(void)
{
      strerr_die2x(111,FATAL,"out of memory");
}

char *dir;
char *user;
char *loguser;
struct passwd *pw;
char *myip;
char *ns;

int main(int argc, char **argv)
{
  int i;
  stralloc tmp = {0,0,0};
  char c;
    
  user = argv[1];
  if (!user) usage();
  loguser = argv[2];
  if (!loguser) usage();
  dir = argv[3];
  if (!dir) usage();
  if (dir[0] != '/') usage();
  myip = argv[4];
  if (!myip) usage();
  ns = argv[5];
  if (!ns) usage();

  pw = getpwnam(loguser);
  if (!pw)
    strerr_die3x(111,FATAL,"unknown account ",loguser);

  init(dir,FATAL);
  makelog(loguser,pw->pw_uid,pw->pw_gid);

  makedir("env");
  perm(02755);
  start("env/ROOT"); outs(dir); outs("/root\n"); finish();
  perm(0644);
  start("env/IP"); outs(myip); outs("\n"); finish();
  perm(0644);
  for(i = 0; i<10; i++)
  {
     if (!argv[5+i]) break;
     if (!stralloc_copys(&tmp,"env/NS")) nomem();
     c='0'+i;
     if (!stralloc_append(&tmp,&c)) nomem();
     if(!stralloc_0(&tmp)) nomem();
     start(tmp.s);
     outs(argv[5+i]);
     outs("\n");
     finish();
     perm(0644);
  }
  start("env/PGHOST"); outs("\n"); finish();
  perm(0644);
  start("env/PGUSER"); outs("\n"); finish();
  perm(0644);
  start("env/PGPASSWORD"); outs("\n"); finish();
  perm(0600);
  start("env/PGDATABASE"); outs("\n"); finish();
  perm(0644);

  start("run");
  outs("#!/bin/sh\n"
       "exec 2>&1\n"
       "rm -f root/tmp/.s.PGSQL.*\n"
       "ln /tmp/.s.PGSQL.* root/tmp\n"
       "exec envuidgid "); outs(user);
  outs(" envdir ./env softlimit -d2000000 ");
  outs(auto_home); outs("/bin/pgsqldns\n");
  finish();
  perm(0755);

  makedir("root");
  perm(02755);

  makedir("root/tmp");
  perm(0755);
  
  _exit(0);
}
