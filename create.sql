-- pgsqldns makes use of two tables, domain and entry.
-- These tables may contain any additional fields.
-- Server needs only SELECT access to these tables

-- If you want to create tables in another schema, be sure to
-- use SQL_INITIALIZE environment variable in server for setting
-- the right path to the tables.
set search_path = 'public';

create table domain(
-- must contain at least the following fields:
  id INT PRIMARY KEY,
  name TEXT NOT NULL
);

create table entry( 
-- must contain at least the following fields:
  prefix TEXT NOT NULL,
  domain INT NOT NULL REFERENCES domain(id),
  type INT NOT NULL,
  ttl INT NOT NULL,
  timestamp TIMESTAMP,
  ip INET,
  distance INT,
  name TEXT
);

--If timestamp is set and ttl is zero, the TTL sent in the record is set
--such that the record will expire no later than the given time, and after
--the given time is reached the record is no longer served out. If timestamp
--is set and ttl is not zero, the record is only served out after the given
--time (with its TTL as indicated).

--type must be one of the following:
--  * 1  == A (address). The ip  field are used.
--  * 12 == PTR (reverse pointer) to name field
--  * 15 == MX (mail exchanger). The distance and name fields are used.
--  * 16 == TXT (Text). The name field is used as the text string.
-- All other types are currently ignored.

create index domain1 on domain(name);
create index entry1 on entry(prefix,domain);
create index entry2 on entry(ip); 

---------------------------
-- tables for sql dns cache
---------------------------
create table dnscache(
  domain TEXT NOT NULL,
  type INT2 NOT NULL,
  expire TIMESTAMP without time zone NOT NULL,
  ip INET,
  distance INT2,
  name TEXT
) WITH OIDS;

CREATE OR REPLACE FUNCTION cleandnscache(bool) returns numeric as '
DECLARE
 rows integer;
 total integer;
 expirerecords alias for $1;
BEGIN
total := 0;
-- Delete expired records only if user requests it
IF expirerecords = TRUE THEN
   DELETE FROM dnscache WHERE dnscache.expire<now();
   GET DIAGNOSTICS rows = ROW_COUNT;
   total := total + rows;
END IF;
-- Keep only latest inserted version of each record
DELETE FROM dnscache WHERE NOT oid IN (SELECT max(oid) FROM dnscache GROUP
BY domain,type,ip,name,distance);
GET DIAGNOSTICS rows = ROW_COUNT;
total := total + rows;
-- delete stale records (query dont delete all these records)
DELETE FROM dnscache WHERE NOT expire IN (SELECT max(expire) FROM dnscache GROUP by domain,type);
GET DIAGNOSTICS rows = ROW_COUNT;
total := total + rows;
RETURN total;
END;
'
language 'plpgsql';
